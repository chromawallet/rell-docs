Type conversions
==========================

Below here is a table to visualize the type conversions that happens in the the Rell backend to its corresponding Gtv types when inserting into, and querying the types.

.. list-table::
   :header-rows: 1

   * - Types
     - Input
     - Output
     - Remark
   * - entity		
     - GtvInteger
     - GtvInteger
     - 
   * - enum
     - GtvInteger
     - GtvInteger
     - Can be GtvString as input to query
   * - struct
     - GtvArray
     - GtvDict
     - Can be GtvDict as input to query
   * - decimal	
     - GtvString
     - GtvString
     - 
   * - boolean			
     - GtvInteger
     - GtvInteger
     -
   * - rowid
     - GtvInteger
     - GtvInteger
     - 
   * - json
     - GtvString
     - GtvString
     - 
   * - nullable	
     - GtvNull or type
     - GtvNull or type
     - 
   * - collection
     - GtvArray
     - GtvArray
     - Both set and list
   * - map				
     - GtvDict
     - GtvDict
     - If key is text
   * - map
     - GtvArray
     - GtvArray
     - If key is not text [[k1, v1], [k2, v2], ...]
   * - tuple
     - GtvDict
     - GtvDict
     - If named "(x = 1, y = 2, z = 3)"
   * - tuple
     - GtvArray
     - GtvArray
     - If not named "(1, 2, 3)"