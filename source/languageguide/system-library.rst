========================
System Library (Globals)
========================

.. contents:: Table of Contents

-----

Entities
--------

.. code-block:: rell

    entity block {
        block_height: integer;
        block_rid: byte_array;
        timestamp;
    }

    entity transaction {
        tx_rid: byte_array;
        tx_hash: byte_array;
        tx_data: byte_array;
        block;
    }

It is not possible to create, modify or delete values of these entities in code.

--------------

Namespaces
----------

chain_context
~~~~~~~~~~~~~

| ``chain_context.args: module_args`` - module arguments specified in ``run.xml``.
    The type is ``module_args``, which must be a user-defined struct. If no ``module_args`` struct is defined in the module,
    the ``args`` field cannot be accessed.

Example of ``module_args``:

.. code-block:: rell

    struct module_args {
        name: text;
        age: integer;
    }

Corresponding module configuration:

.. code-block:: rell

    <run wipe-db="true">
        <chains>
            <chain name="module-args-example" iid="0">
                <config height="0">
                    <app module="example">
                        <arg key="name"><string>Alice</string></arg>
                        <arg key="age"><integer>46</integer></arg>
                    </app>
                </config>
            </chain>
        </chains>
    </run>

Code that reads ``module_args``:

.. code-block:: rell

    function f() {
        print(chain_context.args.name);
        print(chain_context.args.age);
    }

Every module can have its own ``module_args``. Reading ``chain_context.args`` returns the args for the current module, and
the type of ``chain_context.args`` is different for different modules: it is the ``module_args`` struct defined in that module.

| ``chain_context.blockchain_rid: byte_array`` - blockchain RID

| ``chain_context.raw_config: gtv`` - blockchain configuration object, e. g. ``{"gtx":{"rell":{"mainFile":"main.rell"}}}``

op_context
~~~~~~~~~~

System namespace ``op_context`` can be used only in an operation or a function called from an operation, but not in a query

- ``op_context.block_height: integer`` - the height of the block currently being built, equivalent to ``op_context.transaction.block.block_height``

- ``op_context.last_block_time: integer`` - the timestamp of the last block, in milliseconds (like ``System.currentTimeMillis()`` in Java). Returns ``-1`` if there is no last block (the block currently being built is the first block)

- ``op_context.op_index: integer`` - Index of the operation being executed in the transaction (0 == first operation)

- ``op_context.get_signers(): list<byte_array>`` - Returns pubkeys of the signers of the current transaction

- ``op_context.is_signer(pubkey: byte_array): boolean`` - Checks if the pubkey is one of the signers of the current transaction
- ``op_context.get_all_operations(): list<gtx_operation>`` - Returns all operations of the current transaction.

- ``op_context.transaction: transaction`` - the transaction currently being built.

crypto
~~~~~~~~~~

Namespace used for cryptographic functions.

| ``crypto.keccak256(byte_array): byte_array`` - cryptographic hash functions
| ``crypto.sha256(byte_array): byte_array``
  
  

--------------

Global Functions
----------------

- ``abs(integer): integer`` - absolute value of an integer
- ``abs(decimal): decimal`` - absolute value of a decimal

- ``empty(T?): boolean`` - returns ``true`` if the argument is ``null`` or an empty collection and ``false`` otherwise; for nullable collections checks both conditions
- ``empty(list<T>): boolean``
- ``empty(set<T>): boolean``
- ``empty(map<K, V>): boolean``

- ``exists(T?): boolean`` - opposite to ``empty()``
- ``exists(list<T>): boolean``
- ``exists(set<T>): boolean``
- ``exists(map<K, V>): boolean``

- ``log(...)`` - print a message to the log (same usage as ``print``)

- ``max(integer, integer): integer`` - maximum of two integer values
- ``max(decimal, decimal): decimal`` - maximum of two decimal value

- ``min(integer, integer): integer`` - minimum of two integer values
- ``min(decimal, decimal): decimal`` - minimum of two decimal values

- ``text.like(pattern): boolean`` - simple pattern matching function, equivalent to the SQL LIKE clause. Special character "_" matches any single character and "%" matches any string of zero or more characters.

Example of a like function
~~~~~~~~~~~~~~~~~~~~~~~~~~
- ``print(name.like(% von %))`` - returns all names that have a von inside
- ``user @* {name.like(Vi_tor)}`` - returns all users that have one character between Vi and tor (e.g Victor or Viktor)
- ``print(...)`` - print a message to STDOUT:

-  ``print()`` - prints an empty line
-  ``print('Hello', 123)`` - prints ``"Hello 123"``

- ``verify_signature(message: byte_array, pubkey: pubkey, signature: byte_array): boolean`` - returns ``true`` if the given signature is a result of signing the message with a private key corresponding to the given public key.

--------------

Require function
-----------------

**Checking a boolean condition**

- ``require(boolean[, text])`` - throws an exception if the argument is ``false``

**Checking for** ``null``

- ``require(T?[, text]): T`` - throws an exception if the argument is ``null``, otherwise returns the argument

- ``require_not_empty(T?[, text]): T`` - same as the previous one

**Checking for an empty collection**

- ``require_not_empty(list<T>[, text]): list<T>`` - throws an exception if the argument is an empty list, otherwise returns the list

- ``require_not_empty(set<T>[, text]): set<T>`` - throws an exception if the argument is an empty set, otherwise returns the set

- ``require_not_empty(map<K,V>[, text]): map<K,V>`` - throws an exception if the argument is an empty map, otherwise returns the map

When passing a nullable collection to ``require_not_empty``, it throws an
exception if the argument is either ``null`` or an empty collection.

**Examples**

.. code-block:: rell

    val x: integer? = calculate();
    val y = require(x, "x is null"); // type of "y" is "integer", not "integer?"

    val p: list<integer> = get_list();
    require_not_empty(p, "List is empty");

    val q: list<integer>? = try_to_get_list();
    require(q);           // fails if q is null
    require_not_empty(q); // fails if q is null or an empty list
