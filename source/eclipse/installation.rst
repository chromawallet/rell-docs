Installation
============

Prerequisites
-------------

* Java 11+.
* To be able to run a Postchain node: PostgreSQL 10 or later.
* Eclipse IDE (Eclipse can be downloaded `here <https://www.eclipse.org/downloads/packages/>`_.)

Adding the Rell plugin to the Eclipse IDE
-----------------------------------------

Once you have downloaded the Eclipse IDE, the next step is to add the Rell plugin. 

#. Go to the menu **Help** - **Install New Software...**
#. In the Install dialog, click **Add...**, then type:

   * Name: Rell
   * Location: https://www.chromia.dev/rell-eclipse/update


.. image:: images/Eclipse-add_Rell.PNG

#. Rell shall appear in the list. Select it and click **Next >**, then **Finish**.

   .. image:: images/Rell-install.PNG
#. If a warning "You are installing software that contains unsigned content" appears, click **Install anyway**.
#. Click **Restart Now** when asked to restart Eclipse IDE.
#. Switch to the Rell perspective. Menu **Window** - **Perspective** - **Open Perspective** - **Other...**, choose **Rell**.

.. image:: images/Eclipse-perspective.png

Enabling automatic updates for the plugin
----------------------------------------------

When a new version of the Rell plugin is released, it has to be updated in Eclipse. If Rell update URL has already been configured, Eclipse will check for
updates automatically once in a while, and show a message when a plugin can be updated.

To manually check for updates:

#. Menu: **Help** - **Check for Updates**.
#. If it shows that a new version of Rell plugin is available, install it.

If "No updates found" message is shown, check that Rell update site is set up.

.. image:: images/Eclipse-update.png   

#. Click **available software sites** link in the message dialog.
#. If there is no Rell in the list, click **Add...** to add it.

   .. image:: images/Eclipse-software_sites.png
#. Specify:

   * Name: Rell
   * Location: https://www.chromia.dev/rell-eclipse/update

   and click **Add**.

If Rell update site is in the list, but "No updates found" message is shown, try to reload the site:

#. Click **available software sites** link in the message dialog.
#. Click **Reload**.

If still no updates are shown, your Rell plugin must be already up-to-date.