=========================
Eclipse IDE
=========================

Rell has a plugin for Eclipse which can be used to write Rell code and starting up a testnode with minimal hassle.
In this chapter we will be introduced to this plugin, how to write a Rell program with it and how to launch a testnode with it.

.. toctree::
  :maxdepth: 1

  installation
  database-setup
  writing-rell
  ide-features
