Using Rell in Eclipse
======================

Now that we have everything installed, we should try to make a simple hello world in Rell.

Hello World in Rell
--------------------

#. Switch to the Rell perspective, if not done already. Menu: **Window** - **Perspective** - **Open Perspective** - **Other...**, choose **Rell**.
#. Create a project:

   * Menu **File** - **New** - **Rell Project**.
   * Enter a project name ``test`` and click **Finish**.

   .. image:: images/Rell-project.png   

   A Rell project with a default directory structure shall be created.
#. Open the module.rell file:

   * click on the project folder ``test`` folder and then ``src``  - ``test``. 
   * Click on the ``module.rell`` file to open the editor
#. Write following code in the editor:

   ::

       function main() {
           print('Hello, World!');
       }


   Save: **File** - **Save** or CTRL-S (⌘S).

#. Run the program: right-click on the editor and choose **Run As** - **Rell Console App**.

   .. image:: images/Rell-run-as.png
#. The output "Hello, World!" should be shown in the Console view.

IDE Overview
-------------

Eclipse IDE window consists of different views. Every view has its own tab. By default, Rell IDE has following views:

* **Project Explorer** - shows projects and their directory trees.
* **Problems** - shows compilation warnings and errors.
* **Console** - console output (when running programs).
* **Outline** - shows the structure of a selected Rell file.

.. image:: images/Eclipse-overview.png

Running Applications
---------------------

Right-click on a file (or an editor) and choose **Run As**. Run options available for the file will be shown.

.. image:: images/Eclipse-running-applications.png

Alternatively, use keyboard shortcut CTRL-F11 (⇧⌘F11).

Rell Console App
----------------

Executes a ``*.rell`` file as a stand-alone console program, not as a module in a Postchain node.

The program must contain a function (or operation, or query) called ``main``, which will be the entry point. The output is displayed in the Console view.

The name of the main function and its arguments can be specified in a `run configuration <run-configurations_>`_.


Database connection
~~~~~~~~~~~~~~~~~~~

By default, the program is executed without a database connection, and an attempt to perform a database operation will result in a run-time error.

To run a console app with a database connection, there must be a file called ``console-db.properties``, ``db.properties`` or ``node-config.properties``
in the directory of the Rell file or in the ``rell/config`` directory of the project. The file shall contain database connection settings. For example:

.. code-block:: none

    database.driverclass=org.postgresql.Driver
    database.url=jdbc:postgresql://localhost/postchain
    database.username=postchain
    database.password=postchain
    database.schema=rell_app

.. image:: images/Eclipse-database.png

When running a console app with a database connection, tables for defined classes and objects are created on start-up. If a table already exists, missing
columns are added, if necessary.

How to prepare a database is described in the :ref:`database-setup` section.


Rell Postchain App
------------------

Starts a Postchain node with a configuration written in the `Run.XML <../runxml.html>`_ format.
To use this option, right-click on a ``*.xml`` file, not on a ``*.rell`` file.

Using run.xml gives you the option to run multiple blockchains in one Postchain node.

Example of a minimal run.xml:

.. code-block:: xml

        <run>
            <nodes>
                <config src="node-config.properties" add-signers="true" />
                <test-config src="node-config-test.properties"/>
            </nodes>
            <chains>
                <chain name="test" iid="1">
                    <config height="0">
                        <app module="test">
                        </app>
                    </config>
                </chain>
            </chains>
        </run>
  

.. image:: images/Eclipse-runxml.png


Wipe Database
-------------

This option is available for database properties files, ``*.properties`` (for instance, ``node-config.properties``).
Drops all tables (and stored procedures) in the database.

.. image:: images/wipe-database.png


Run with a Keyboard
-------------------

By default, CTRL-F11 (⇧⌘F11) shortcut runs the file of the active editor. It can be configured to run the last launched application instead, which may be more convenient,
as there is no need to choose an application type. Go to the menu **Window** - **Preferences** (macOS: **Eclipse** - **Preferences**), then **Run/Debug** - **Launching**.
In the **Launch Operation** box, choose **Always launch the previously launched application**.

.. image:: images/Eclipse-runwithkeyboard.png


Running Multiple Apps
---------------------

It is possible to run multiple applications (e. g. multiple nodes) simultaneously.
For example, one can define two `Run.XML <../runxml.html>`_ configuration files that use the same Rell module, but different ports and database schemas.

The output of all running applications will be shown in the Console view, but on different pages.
It is possible to switch between the consoles of different applications using a button or a dropdown list.

.. image:: images/Eclipse_multiple_apps.png

.. _run-configurations:

Run Configurations
------------------

To run an application, Eclipse IDE needs a run configuration, which contains different properties, like the name of the main function, arguments or
blockchain RID.
When running an application via the **Run As** context menu, the IDE automatically creates a run configuration with default settings if it does not exist.

To change a run configuration, go to the menu **Run** - **Run Configurations...**. The last launched application will be selected.
Change the settings and click either **Apply** or **Run** to save the changes.

.. image:: images/Eclipse-runconfig.png

