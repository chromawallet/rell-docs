# Readme

This is the Chromia docs repo. 
The site is hosted via Read the Docs. 

Rell docs use reStructuredText as the markup language and its documentation can be found [here](http://docutils.sourceforge.net/rst.html).
Sphinx is used to build the .rst files into static HTML sites.


## Read the Docs

The site is hosted on Read the Docs servers, and reachable at rell.readthedocs.io or at rell.chromia.com.
When changes are pushed to master, the documentation will automatically build and be available after a few minutes.

## Admin

The Developer Experience team maintains this repo. The product owner is Erik Frisk. Speak to him if you need something.

## Setup

### Setup for Windows

1. Download Python from Microsoft Store if it's not already installed.

2. Download pip from pips [website](https://pip.pypa.io/en/latest/installation/) if it's not already installed. 

3. Clone [the rell-docs repo](https://bitbucket.org/chromawallet/rell-docs/src/master/) to where you want to have it locally.

While located inside the rell-docs directory:

4. Run: `pip install virtualenv` to install virtual ennviroment.

5. `python -m virtualenv env` , to create a virtual enviroment with the name "env".

6. `env/Scripts/activate` to activate the virtual enviroment.

7. `pip install sphinx`

8. `pip install sphinxsearch`

9. `pip install readthedocs-sphinx-search`

10. `pip install sphinx-rtd-theme`

### Setup for Linux/Mac

1. Clone [this repo](https://bitbucket.org/chromawallet/rell-docs/src/master/) to where you want to have it locally.

While located inside the rell-docs directory:

2. `pip install sphinx`

3. `pip install sphinxsearch`

4. `pip install readthedocs-sphinx-search`

5. `pip install sphinx-rtd-theme`


### Build and run locally

1. While located in the 'rell-docs' directory in the terminal write: `./sphinx-build.sh`

2. Open the generated html file in your browser: `open build/index.html` or open `index.html` from the build folder.