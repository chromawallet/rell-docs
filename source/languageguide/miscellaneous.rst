
Miscellaneous
=============

Comments
--------

Single-line comment:

.. code-block:: rell

    print("Hello"); // Some comment

Multiline comment:

.. code-block:: rell

    print("Hello"/*, "World"*/);
    /*
    print("Bye");
    */
