==========
GTV
==========
GTV is a general purpose protocol, used to express data in;

* Primitive types (integers, strings, byte arrays, etc)
* Complex types (only Array and Dictionary)

GTV is converted to ASN when it's sent.

.. code-block:: none 

   GtvMessages DEFINITIONS ::= BEGIN

	DictPair ::= SEQUENCE {
		name UTF8String,
		value RawGtv
	}

	RawGtv ::= CHOICE {
		null [0] NULL,
		byteArray [1] OCTET STRING,
		string [2] UTF8String,
		integer [3] INTEGER,
		dict [4] SEQUENCE OF DictPair,
		array [5] SEQUENCE OF RawGtv,
		bigInteger [6] INTEGER
	} 
   END