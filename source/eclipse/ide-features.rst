Features of the IDE
===================

Outline
-------

When a Rell editor is open, the structure of its file (tree of definitions) is shown in the Outline view (which is by default on the right side of the IDE).

.. image:: images/Eclipse-outline.png

There is also a Quick outline window, which is activated by the CTRL-O (⌘O) shortcut. Type a name to find its definition in the file.

.. image:: images/outline_popup_ed.png
.. image:: images/outline_popup_4.png


Problems View
-------------

Problems view shows compiler warnings and errors found in all projects in the IDE. The list is updated when saving a file.

.. image:: images/problems_view.png


Global Text Search
------------------

Press CTRL-H (⌘H) to open the Search dialog. It allows to search for a string in all files in the IDE. Select the **File Search** tab, enter the text to search for
and file name pattern.

.. image:: images/search_dialog.png

Results are shown in the Search view, as a tree.

.. image:: images/search_results.png


Git
-------------

Git operations are available via a context menu: right-click on a file and choose **Team**.

.. image:: images/git_menu_ed.png

To commit file(s), click **Add to Index**, then **Commit..**.

File icon in the Project Explorer indicates whether the file is a new, changed or an unmodified file.

.. image:: images/git_files_ed.png

Changed files do not look very nice. To change the way how they are displayed, go to the **Window** - **Preferences** (macOS: **Eclipse** - **Preferences**) menu, then
**Team** - **Git** - **Label Decorations**:

* on the **Text Decorations** tab: delete the ">" character in all text fields

   .. image:: images/git_preferences_text_decorations_ed.png
* on the **Icon Decoartions** tab: check the **Dirty resources** checkbox

   .. image:: images/git_preferences_icon_decorations_ed.png
* click **Apply and Close**

Now files look much better:

.. image:: images/git_files_4_ed.png

Spaces vs. Tabs
---------------

Eclipse uses tabs instead of spaces by default. It is recommended to use spaces. A few settings have to be changed:

#. Open the Preferences dialog: menu **Window** - **Preferences** (macOS: **Eclipse** - **Preferences**).
#. Type "tabs" in the search box.
#. Go to **General** - **Editors** - **Text Editors** and check **Insert spaces for tabs**.

   .. image:: images/preferences_text_editors_ed.png
#. Go to **XML** - **XML Files** - **Editor**, select **Indent using spaces**, specify **Indentation size**: 4.

   .. image:: images/preferences_xml_editor_ed.png


Miscellaneous
-------------

* To show or hide line numbers: right-click on the left margin of an editor, click **Show Line Numbers**.

   .. image:: images/menu_show_line_numbers_ed.png

* To comment a code fragment, select it and press CTRL-/ (⌘/).

* Activate the *Link with Editor* icon, and the IDE will automatically select a file in the Project Explorer when its editor is focused.

   .. image:: images/link_with_editor_ed.png

* CTRL-SHIFT-R (⇧⌘R) invokes the Open Resource dialog, which allows to search project files by name. Glob patterns (with ``*`` and ``?``) are supported.

   .. image:: images/open_resource_dialog_ed.png

* The *Show Whitespace Characters* (paragraph) icon in the main toolbar allows to distinguish tabs from spaces.

   .. image:: images/Eclipse-whitespace.png

* To see the full list of Eclipse keyboard shortcuts, press CTRL-SHIFT-L (⇧⌘L). Some shortcuts are for the Java editor, and do nothing in Rell.

   .. image:: images/keyboard_shortcuts_ed.png

Keyboard shortcuts
------------------

Most useful keyboard shortcuts (subjectively):

========================================================================= =============== =============
Action                                                                    Linux/Windows   macOS
========================================================================= =============== =============
Run the file shown in the active editor                                   CTRL-F11        ⇧⌘F11
|
Show the New wizard                                                       CTRL-N          ⌘N
Show the New menu                                                         ALT-SHIFT-N     ⌥⌘N
Save the current file                                                     CTRL-S          ⌘S
Close the active editor tab                                               CTRL-W          ⌘W
Close all editor tabs                                                     CTRL-SHIFT-W    ⇧⌘W
|
Quick outline                                                             CTRL-O          ⌘O
Find text in the active editor                                            CTRL-F          ⌘F
Globally search for the selected text fragment                            CTRL-ALT-G      ⌥⌘G
Show global text search dialog                                            CTRL-H          ^H
Find a file by name                                                       CTRL-SHIFT-R    ⇧⌘R
|
Go to a line number                                                       CTRL-L          ⌘L
Go to a previous location                                                 ALT-Left        ⌥⌘←
Go to a next location (can be used after Alt-Left)                        ALT-Right       ⌥⌘→
Go to the last edit location                                              CTRL-Q          ⌘Q
|
Comment the selected code fragment with ``//``                            CTRL-/          ⌘/
Convert selected text to upper case                                       CTRL-SHIFT-X    ⇧⌘X
Convert selected text to lower case                                       CTRL-SHIFT-Y    ⇧⌘Y
|
Show the full list of keyboard shortcuts                                  CTRL-SHIFT-L    ⇧⌘L
========================================================================= =============== =============

