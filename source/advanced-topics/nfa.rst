==========
NFA Module
==========

Non-Fungible Assets (NFA) is the Chromia standard used to create unique and trackable assets.

NFAs can be dynamically created on both client and blockchain side. We will explain each part after the basic structures:

.. toctree::
  :maxdepth: 1

  nfa/nfa-basic-structures
  nfa/nfa-blockchain-side
  nfa/nfa-client-side
