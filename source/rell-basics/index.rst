=========================
Rell Basics
=========================

This chapter will cover basic Rell syntax and functionality, as well as how we write our 
client side so that it can communicate with our blockchain. Rell is a language designed for 
relational blockchain programming. The structure of a decentralized application built 
in Rell will look something like this: 

.. image:: structure.png


The end user will be communicating with our client side, which in turn will send transactions 
to Rell using a postchain client. There are currently postchain clients for JavaScript, C# and 
Java, and we will be using JavaScript for our client side example.


- The :doc:`Main Concepts <./main-concepts>` section guides you through the concepts needed to create a program in Rell.
- While :doc:`Client Side <./main-concepts>` describes how to work with a Rell backend using a JavaScript client.

.. toctree::
   :hidden:

   main-concepts
   client-side
