====================
Postchain REST API
====================

Postchain is the blockchain framework that Chromia is built on. In the Chromia ecosystem, Providers 
run Postchain nodes that form the infrastructure for the network. Sometimes it makes sense to run 
your own node or set of nodes, such as for testing, development, or even for setting up your own 
consortium blockchain network. 

Postchain nodes expose a REST API that allows for submitting transactions, posting queries, fetching 
block info, reading node status, etc. You can |postchain_rest_api_link|.

.. |postchain_rest_api_link| raw:: html

  <a href="../_static/postchain-restapi.html" target="_blank">read the full API documentation here</a>
