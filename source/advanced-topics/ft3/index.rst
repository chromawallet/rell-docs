==========
FT3 Module
==========

FT3 is the recommended standard to handle accounts and tokens in the Chromia ecosystem. It allows dapps to make full use of Chromia Vault, including Single Sign-On (SSO), asset transfers and visibility on the Vault's dapp explorer.

FT3 consists of a Rell module that contains blockchain logic, and a client side library that provides a JS API for interaction with the module.

.. toctree::
   :maxdepth: 1

   ft3-features
   ft3-project-setup
   ft3-javascript-library
   ft3-rell-ft3-integration
   ft3-single-sign-on
   ft3-updating-a-chain

.. ft3-chromia-chat
