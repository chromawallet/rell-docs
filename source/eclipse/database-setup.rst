Database Setup
==============

.. _database-setup:

Database Setup
--------------

Rell requires PostgreSQL 10 to be installed and set up. The IDE can work without it, but will not be able to run a node.
A console app or a remote postchain app can be run without a database, though.

Default database configuration for Rell is:

* database: ``postchain``
* user: ``postchain``
* password: ``postchain``

Ubuntu (Debian)
~~~~~~~~~~~~~~~~

Install PostgreSQL:

.. code-block:: none

    sudo apt-get install postgresql

Prepare a Postgres database:

.. code-block:: none

    sudo -u postgres psql -c "CREATE DATABASE postchain;" -c "CREATE ROLE postchain LOGIN ENCRYPTED PASSWORD 'postchain'; GRANT ALL ON DATABASE postchain TO postchain;"


MacOS
~~~~~

Install PostgreSQL:

.. code-block:: none

    brew install postgresql
    brew services start postgresql
    createuser -s postgres

Prepare a Postgres database:

.. code-block:: none

    psql -U postgres -c "CREATE DATABASE postchain;" -c "CREATE ROLE postchain LOGIN ENCRYPTED PASSWORD 'postchain'; GRANT ALL ON DATABASE postchain TO postchain;"

.. note::
        If you get an error saying that peer authentication failed, you will have to change authentication method from peer to md5. this can be done inside the pg_hba.conf file of your psql database.


Docker
~~~~~~

.. code-block:: none

    docker run --name postchain -e POSTGRES_USER=postchain -e POSTGRES_PASSWORD=postchain -p 5432:5432 -d postgres