==========
GTX
==========
GTX is a protocol that Postchain uses for sending and receiving transactions.
GTX is encoded into the base protocol GTV.

It's not recommended to use GTV directly, even though it is possible to construct various Rell
types directly into GTV.

.. code-block:: none
   
   GtxMessages DEFINITIONS ::= BEGIN

   IMPORTS RawGtv FROM GtvMessages;

    -- All types are using the same tags as RawGtv to make gtv<->gtx encoding equivalent

    -- Gtx operation
    --  [ string, [ gtv ] ]
	RawGtxOp ::= [5] EXPLICIT SEQUENCE {
		name [2] EXPLICIT UTF8String, -- RawGtv { string } --
		args [5] EXPLICIT SEQUENCE OF RawGtv -- RawGtv { array } --
	}

    -- Gtx Body
    -- [ bytearray, [ gtxOp ], [ bytearray ] ]
	RawGtxBody ::= [5] EXPLICIT SEQUENCE {
		blockchainRid [1] EXPLICIT OCTET STRING, -- RawGtv { bytearray } --
		operations [5] EXPLICIT SEQUENCE OF RawGtxOp,
		signers [5] EXPLICIT SEQUENCE OF [1] EXPLICIT OCTET STRING -- RawGtv { bytearray} --
	}

	-- Gtx
	-- [ gtxBody, [ bytearray] ]
	RawGtx ::= [5] EXPLICIT SEQUENCE {
	    body RawGtxBody,
		signatures [5] EXPLICIT SEQUENCE OF [1] EXPLICIT OCTET STRING -- RawGtv { bytearray } --
	}
   END


.. toctree::
   :maxdepth: 1

   gtv
   rell-to-gtv-conversions
